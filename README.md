# Team Flat Map

Flat_Map is designed for software systems with data that is continuously changing and requires fast and efficient updates. Flat_Map is similar to the std::Map but it is implemented like an ordered sorted vector.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Files

#### Source Files
main.cpp, flat_map.hpp, flat_map.cpp, hello_world.cpp, performance.cpp, taxi.cpp

#### Analysis
erase_distribution.xlsx, find_distribution.xlsx, insert_distribution.xlsx

#### Excel
bino100.csv, bino1000.csv, bino10000.csv, bino100000.csv, disc100.csv ...

## Compile Instruction

There are several executive files can be generate from Makefile.

First, run this in your terminal:

```
cd src
make all
```

run this inside src directory to check out functions in flat map library:

```
./main
```

run this to get peformance of functions: 

```
./performance
```

run this to get binomial distribution runtime with 100 elements (see reference table):

```
 ./performance 1 100
```

run this to perform "hello word": 

```
./hello
```

run this to read yellow_tripdata_2018-05-166.csv file into a flat map: 

```
./read_csv
```

### Store Results to csv files

Store 100 results of a binomial distribution flat map with 100 elements to a csv file:

```
cd src
./performance.sh
cat ./../excel/bin100.csv
```

Run all distributions and store them seperately in different csv file (should take a while):

```
cd src
./performanceofall.sh
cd ./../excel
```

## Reference Table

### $1 first argument options

* 0   binomial_distribution 

* 1   discrete_distribution 

* 2   extreme_value_distribution 

* 3   fisher_f_distribution 

* 4   gamma_distribution 

* 5   lognormal_distribution 

* 6   negative_binomial_distribution 

* 7   normal_distribution 

* 8   uniform_int_distribution 

* 9   uniform_real_distribution 

* 10  weibull_distribution 

### $2 second argument options

* 100     one hundred elements 

* 1000    one thousand elements 

* 10000   ten thousand elements 

## Running the tests

Test files are stored in __testcases__ directory.

run this to perform tests: 

```
cd __testcases__
make all
./test1
./test2
./test3
./test4
./test5
```

### Capacity

Test1 is design to test the capacity of flat map. The result shows the maximum potential size the container that can reach due to known system or library implementation limitations, but the container is by no means guaranteed to be able to reach that size: it can still fail to allocate storage at any point before that size is reached.

```
Testing Capacity Functions
2305843009213693951
tests passed!
```

### Constructor

Test2 is design to test standard constructor and copy constructor.

### Element Access

Test3 is design to test modifier operations.

### Iterators

Test4 is design to test iterators.

### Lookup

Test5 is design to test lookup. 

### Modifiers

Test6 is design to test modifiers operations. 


## Built With

* [Xcode] -Version 9.4.1 (9F2000)
* [Visual Studio Code]-2018

## Hardware

* Processor: 2.8 GHz Intel Core i7    

* Memory: 16 GB 2133 MHz LPDDR3 

* Mac OS

* Develop Environment: VS code and Xcode

## gcc --version

* Configured with: --prefix=/Library/Developer/CommandLineTools/usr --with-gxx-include-dir=/usr/include/c++/4.2.1

* Apple LLVM version 9.1.0 (clang-902.0.39.2)

* Target: x86_64-apple-darwin17.4.0

* Thread model: posix

* InstalledDir: /Library/Developer/CommandLineTools/usr/bin

## Authors

**Adiza Awwal**, 
**Ayer Chan**, 
**Tahiya Chowdhury** 

