#include <stdio.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <iomanip>
#include "flat_map.hpp"
#include <map>
#include <unordered_map>

flat_map<int, std::unordered_map<std::string, std::string>> read_csv(flat_map<int, std::unordered_map<std::string, std::string>>& fmap, std::string filename)
{
    std::fstream fin;
    fin.open(filename, std::ios::in);
    int roll2, count = 0;
    std::vector<std::string> row;
    std::string line, word, temp;
    
    while (fin >> temp) {
        row.clear();
        getline(fin, line);
        std::stringstream s(line);
        
        while (std::getline(s, word, ',')) {
            row.push_back(word);
        }
        
        roll2 = stoi(row[0]);
        
        /*******************************************/
        /*                                         */
        /*      taxi info storage starts here      */
        /*                                         */
        /*******************************************/
        
        std::unordered_map<std::string, std::string> eachtaxi;
        eachtaxi.insert(std::make_pair("VendorID", row[0]));
        eachtaxi.insert(std::make_pair("tpep_pickup_datetime", row[1]));
        eachtaxi.insert(std::make_pair("tpep_dropoff_datetime", row[2]));
        eachtaxi.insert(std::make_pair("passenger_count", row[3]));
        eachtaxi.insert(std::make_pair("trip_distance", row[4]));
        eachtaxi.insert(std::make_pair("RatecodeID", row[5]));
        eachtaxi.insert(std::make_pair("store_and_fwd_flag", row[6]));
        eachtaxi.insert(std::make_pair("PULocationID", row[7]));
        eachtaxi.insert(std::make_pair("DOLocationID", row[8]));
        eachtaxi.insert(std::make_pair("payment_type", row[9]));
        eachtaxi.insert(std::make_pair("fare_amount", row[10]));
        eachtaxi.insert(std::make_pair("extra", row[10]));
        eachtaxi.insert(std::make_pair("mta_tax", row[11]));
        eachtaxi.insert(std::make_pair("tip_amount", row[12]));
        eachtaxi.insert(std::make_pair("tolls_amount", row[13]));
        eachtaxi.insert(std::make_pair("improvement_surcharge", row[14]));
        eachtaxi.insert(std::make_pair("total_amount", row[15]));
        
        fmap.insert(std::make_pair(count, eachtaxi));
        
        /*****     modification ends here      *****/
        
        count++;
        
    }
    return fmap;
}

int main()
{
    flat_map<int, std::unordered_map<std::string, std::string>> test3;
    test3 = read_csv(test3, "yellow_tripdata_2018-05-166.csv");
    
    //List all attributes of the 100th taxi passenger of May 2018
    for(const auto& n : test3[99]) {
        std::cout << "[" << n.first << "]    [" << n.second << "]\n";
    }
    
    //Find who pays the highest tips
    std::string key = "tip_amount";
    int highesttip = 0;
    for(std::string::size_type i = 0; i < test3.size(); i++) {
        for(auto& n : test3[i]) {
            if (n.first == key) {
                int num = stoi(n.second);
                if (num > highesttip) {
                    highesttip = num;
                }
            }
        }
    }
    std::cout << highesttip << "\n";
}

