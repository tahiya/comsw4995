#ifndef flat_map_hpp
#define flat_map_hpp

#include <vector>
#include <map>
#include <iostream>
#include <stdio.h>
#include <string>

template<class K, class V>
class flat_map
{
public: 
    typedef std::pair<K, V> pair_type;
    typedef K key_type;
    typedef V value_type;
    std::vector<pair_type> data;

    // customer defined type for flat_map data structure
    typedef typename std::vector<pair_type>::iterator iterator;
    typedef typename std::vector<pair_type>::reverse_iterator rv_iterator;

    struct compare
    {
        bool operator()(const pair_type& a, const pair_type& b)
        {
            return a.first < b.first;
        }

        bool operator()(const pair_type& a, key_type b)
        {
            return a.first < b;
        }

        bool operator()(key_type a, const pair_type& b)
        {
            return a < b.first;
        }

        bool operator() (key_type a, key_type b)
        {
            return a < b;
        }
    };
    
private:

        std::pair<iterator, bool> insert_private(const pair_type& pair)
    {
        iterator it = std::lower_bound(data.begin(), data.end(), pair.first, compare());
        if (it == data.end() || it->first != pair.first)
        {
            iterator new_it = data.insert(it, pair);
            return std::make_pair(new_it, true);
        }
        else
        {
            return std::make_pair(it, false);
        }
    }


public:

    /* Constructors */
    flat_map(std::size_t space = 100){

        data.reserve(space);
    }
    
    flat_map(flat_map& map){

        for(std::string::size_type i = 0; i < map.data.size(); i++){
            data.push_back(map.data[i]);
        }
    }

    bool operator==(flat_map& map)
    {
        return map.data == data;
    }

    std::pair<iterator, bool> insert(const pair_type& pair)
    {
        iterator it = std::lower_bound(data.begin(), data.end(), pair.first, compare());
        if (it == data.end() || it->first != pair.first)
        {
            iterator new_it = data.insert(it, pair);
            return std::make_pair(new_it, true);
        }
        else
        {
            return std::make_pair(it, false);
        }
    }

    std::pair<iterator, bool> emplace(const K& key, const V& value)
    {
        return insert_private(std::make_pair(key, value));

    }

    V& operator[](const K& key)
    {
        iterator it = std::lower_bound(data.begin(), data.end(), key, compare());
        if (it == data.end() || it->first != key)
        {
            iterator new_it = data.insert(it, pair_type(key, V()));
            return new_it->second;
        }
        else
        {
            return it->second;
        }
    }
    
    iterator find(const K& key)
    {
        iterator it = std::lower_bound(data.begin(), data.end(), key, compare() );
        if( it == data.end() || it->first != key)
        {
            return data.end();
        
        } 

        return it;

    }
    
    void erase(const K& element)
    {
        iterator it = std::lower_bound(data.begin(), data.end(), element, compare() );
        if (it != data.end() && it->first == element)
        {
            data.erase(it);
        }
    }

    iterator begin()
    {
        return data.begin();
    }
    
    iterator end()
    {
        return data.end();
    }
    
    rv_iterator rbegin()
    {
        return data.rbegin();
    }
    
    rv_iterator rend()
    {
        return data.rend();
    }
    
    void clear() 
    {
        data.clear();
    }

    bool empty()
    {
        return data.empty();
    }

    std::size_t max_size()
    {
        return data.max_size();
    }

    std::size_t size() 
    {
        return data.size();
    }

    void swap(flat_map& map)
    {
        data.swap(map.data);
    }
    
    V& at(const K& key)
    {
        iterator it = std::lower_bound(data.begin(), data.end(), key, compare());
        if ( it == data.end() || it->first != key){
             throw std::string("This key does not exist within the map!");
        } else {
            return it->second;
        }
    }

    iterator lower_bound(const K& key)
    {
        return std::lower_bound(data.begin(), data.end(), key, compare());
    }

    iterator upper_bound(const K& key)
    {
        return std::upper_bound(data.begin(), data.end(), key, compare());
    }
};

#endif
