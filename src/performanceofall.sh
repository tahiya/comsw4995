#!/bin/bash

for i in {1..100}; do ./main 0 100 >> bino100.csv; done
for i in {1..100}; do ./main 0 1000 >> bino1000.csv; done
for i in {1..100}; do ./main 0 10000 >> bino10000.csv; done
for i in {1..100}; do ./main 0 100000 >> bino100000.csv; done
for i in {1..100}; do ./main 1 100 >> disc100.csv; done
for i in {1..100}; do ./main 1 1000 >> disc1000.csv; done
for i in {1..100}; do ./main 1 10000 >> disc10000.csv; done
for i in {1..100}; do ./main 1 100000 >> disc100000.csv; done
for i in {1..100}; do ./main 2 100 >> extre100.csv; done
for i in {1..100}; do ./main 2 1000 >> extre1000.csv; done
for i in {1..100}; do ./main 2 10000 >> extre10000.csv; done
for i in {1..100}; do ./main 2 100000 >> extre100000.csv; done
for i in {1..100}; do ./main 3 100 >> fish100.csv; done
for i in {1..100}; do ./main 3 1000 >> fish1000.csv; done
for i in {1..100}; do ./main 3 10000 >> fish10000.csv; done
for i in {1..100}; do ./main 3 100000 >> fish100000.csv; done
for i in {1..100}; do ./main 4 100 >> gamma100.csv; done
for i in {1..100}; do ./main 4 1000 >> gamma1000.csv; done
for i in {1..100}; do ./main 4 10000 >> gamma10000.csv; done
for i in {1..100}; do ./main 4 100000 >> gamma100000.csv; done
for i in {1..100}; do ./main 5 100 >> log100.csv; done
for i in {1..100}; do ./main 5 1000 >> log1000.csv; done
for i in {1..100}; do ./main 5 10000 >> log10000.csv; done
for i in {1..100}; do ./main 5 100000 >> log100000.csv; done
for i in {1..100}; do ./main 6 100 >> negbino100.csv; done
for i in {1..100}; do ./main 6 1000 >> negbino1000.csv; done
for i in {1..100}; do ./main 6 10000 >> negbino10000.csv; done
for i in {1..100}; do ./main 6 100000 >> negbino100000.csv; done
for i in {1..100}; do ./main 7 100 >> nor100.csv; done
for i in {1..100}; do ./main 7 1000 >> nor1000.csv; done
for i in {1..100}; do ./main 7 10000 >> nor10000.csv; done
for i in {1..100}; do ./main 7 100000 >> nor100000.csv; done
for i in {1..100}; do ./main 8 100 >> uni100.csv; done
for i in {1..100}; do ./main 8 1000 >> uni1000.csv; done
for i in {1..100}; do ./main 8 10000 >> uni10000.csv; done
for i in {1..100}; do ./main 8 100000 >> uni100000.csv; done
for i in {1..100}; do ./main 9 100 >> unireal100.csv; done
for i in {1..100}; do ./main 9 1000 >> unireal1000.csv; done
for i in {1..100}; do ./main 9 10000 >> unireal10000.csv; done
for i in {1..100}; do ./main 9 100000 >> unireal100000.csv; done
for i in {1..100}; do ./main 10 100 >> wei100.csv; done
for i in {1..100}; do ./main 10 1000 >> wei1000.csv; done
for i in {1..100}; do ./main 10 10000 >> wei10000.csv; done
for i in {1..100}; do ./main 10 100000 >> wei100000.csv; done

for i in *; do (cat $i | tail -1 ; echo)>> result.csv; done




