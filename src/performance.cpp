#include <iostream>
#include "flat_map.hpp"
#include <map>
#include <unordered_map>
#include <chrono>
#include <utility>
#include <algorithm>
#include <stdlib.h>
#include <vector>
#include <random>
#include <cmath>
#include <cstdlib>
#include <climits>

//
// MEASURING FUNCTION PERFORMANCE
//
typedef std::chrono::high_resolution_clock::time_point TimeVar;

#define duration(a) std::chrono::duration_cast<std::chrono::nanoseconds>(a).count()
#define timeNow() std::chrono::high_resolution_clock::now()

template<typename F, typename... Args>
double funcTime(F func, Args&&... args){
    TimeVar t1=timeNow();
    func(std::forward<Args>(args)...);
    return duration(timeNow()-t1);
}

//
// NUMBER OF ELEMENTS IN MAPS
//
int NUMOFELE = 1000;

//
// DISTRIBUTION
//
long DISTRIBUTION = 1;

/*
 0 binomial_distribution
 1 discrete_distribution
 2 extreme_value_distribution
 3 fisher_f_distribution
 4 gamma_distribution
 5 lognormal_distribution
 6 negative_binomial_distribution
 7 normal_distribution
 8 uniform_int_distribution
 9 uniform_real_distribution
 10 weibull_distribution
 */

//
// RANDOM DEVICE
//
std::random_device rd;  // random seed
std::mt19937 gen(rd()); // Initializing Mersnne Twister pseudo-random number generator

//
// DISTRIBUTIONS
//
unsigned int randnum_bino_distribution(){
    // Randomly generate numbers
    std::binomial_distribution<> random(0,1000);
    unsigned ranNum = random(gen); // generate a number
    
    return ranNum;
}

unsigned int randnum_disc_distribution(){
    // Randomly generate numbers
    std::binomial_distribution<> random(0,1000);
    unsigned ranNum = random(gen); // generate a number
    
    return ranNum;
}

unsigned int randnum_extre_distribution(){
    // Randomly generate numbers
    std::extreme_value_distribution<> random(0,1000);
    unsigned ranNum = random(gen); // generate a number
    
    return ranNum;
}

unsigned int randnum_fish_distribution(){
    // Randomly generate numbers
    std::fisher_f_distribution<> random(0,1000);
    unsigned ranNum = random(gen); // generate a number
    
    return ranNum;
}

unsigned int randnum_gamma_distribution(){
    // Randomly generate numbers
    std::gamma_distribution<> random(0,1000);
    unsigned ranNum = random(gen); // generate a number
    
    return ranNum;
}

unsigned int randnum_log_distribution(){
    // Randomly generate numbers
    std::lognormal_distribution<> random(0,1000);
    unsigned ranNum = random(gen); // generate a number
    
    return ranNum;
}

unsigned int randnum_negbino_distribution(){
    // Randomly generate numbers
    std::negative_binomial_distribution<> random(0,1000);
    unsigned ranNum = random(gen); // generate a number
    
    return ranNum;
}

unsigned int randnum_nor_distribution(){
    // Randomly generate numbers
    std::normal_distribution<> random(0,1000);
    unsigned ranNum = random(gen); // generate a number
    
    return ranNum;
}

unsigned int randnum_uni_distribution(){
    // Randomly generate numbers
    std::uniform_int_distribution<> random(0,1000);
    unsigned ranNum = random(gen); // generate a number
    
    return ranNum;
}

unsigned int randnum_unireal_distribution(){
    // Randomly generate numbers
    std::uniform_real_distribution<> random(0,1000);
    unsigned ranNum = random(gen); // generate a number
    
    return ranNum;
}

unsigned int randnum_wei_distribution(){
    // Randomly generate numbers
    std::weibull_distribution<> random(0,1000);
    unsigned ranNum = random(gen); // generate a number
    
    return ranNum;
}

//
// Choose one of these distribution
//
unsigned int randnum(){
    switch (DISTRIBUTION) {
        case 0:
            return randnum_bino_distribution();
            break;
        case 1:
            return randnum_disc_distribution();
            break;
        case 2:
            return randnum_extre_distribution();
            break;
        case 3:
            return randnum_fish_distribution();
            break;
        case 4:
            return randnum_gamma_distribution();
            break;
        case 5:
            return randnum_log_distribution();
            break;
        case 6:
            return randnum_negbino_distribution();
            break;
        case 7:
            return randnum_nor_distribution();
            break;
        case 8:
            return randnum_uni_distribution();
            break;
        case 9:
            return randnum_unireal_distribution();
            break;
        case 10:
            return randnum_wei_distribution();
            break;
        default:
            break;
    }
    return 0;
}

//
// Find: scan through map and find
//
flat_map<int, int> findFlatmap(flat_map<int, int> &fmap){

    for (int i = 0; i < NUMOFELE; i++) {
        fmap.find(randnum());
    }
    return fmap;
}

std::map<int, int> findStdmap(std::map<int, int> &map){

    for (int i = 0; i < NUMOFELE; i++) {
        map.find(randnum());
    }
    return map;
}

std::unordered_map<int, int> findUnomap(std::unordered_map<int, int> &umap){
    
    for (int i = 0; i < NUMOFELE; i++) {
        umap.find(randnum());
    }
    return umap;
}

//
// Erase: scan through map and earse
//
flat_map<int, int> eraseFlatmap(flat_map<int, int> &fmap){

    for (int i = 0; i < NUMOFELE; i++) {
        fmap.erase(randnum());
    }
    return fmap;
}

std::map<int, int> eraseStdmap(std::map<int, int> &map){

    for (int i = 0; i < NUMOFELE; i++) {
        map.erase(randnum());
    }
    return map;
}

std::unordered_map<int, int> eraseUnomap(std::unordered_map<int, int> &umap){
    
    for (int i = 0; i < NUMOFELE; i++) {
        umap.erase(randnum());
    }
    return umap;
}

//
// Insertion: generate maps
//
flat_map<int, int> insertFlatmap(flat_map<int, int> &fmap){
    
    for (int i = 1; i < NUMOFELE; i++) fmap.insert(std::make_pair(randnum(), randnum()));
    
    return fmap;
}

std::map<int, int> insertStdmap(std::map<int, int> &stdmap){
    
    for (int i = 1; i < NUMOFELE; i++) stdmap.insert(std::make_pair(randnum(),randnum()));    ///* generate number between 1 and max */
    
    return stdmap;
}

std::unordered_map<int, int> insertUnomap(std::unordered_map<int, int> &umap){
    
    for (int i = 1; i < NUMOFELE; i++) umap.insert(std::make_pair(randnum(),randnum()));    ///* generate number between 1 and max */
    
    return umap;
}


//
// main
//
int main(int argc, const char * argv[]) {
    if (argc == 3) {
        DISTRIBUTION = atoi(argv[1]);
        NUMOFELE = atoi(argv[2]);
    }
    
    flat_map<int, int> test1;
    std::map<int, int> test2;
    std::unordered_map<int, int> test3;
    
    insertFlatmap(test1);
    findFlatmap(test1);
    
    //Measurement
    std::cout<<funcTime(insertFlatmap,test1)<<","<<funcTime(insertStdmap,test2)<<","<<funcTime(insertUnomap, test3)
        <<","<<funcTime(findFlatmap,test1)<<","<<funcTime(findStdmap,test2)<<","<<funcTime(findUnomap, test3)
        <<","<<funcTime(eraseFlatmap,test1)<<","<<funcTime(eraseStdmap,test2)<<","<<funcTime(eraseUnomap, test3)<<"\n";

    return 0;
}
