//  main.cpp
#include <iostream>
#include "flat_map.hpp"
#include <map>

using namespace std;

int main() {
    /* main.cpp is not the unit test suite but, a driver document for continued testing */

    flat_map<int, int> test1;

    test1.insert(std::make_pair(3, 9));
    test1.insert(std::make_pair(4, 10));
    test1.insert(std::make_pair(5, 11));

    cout << "Testing insert: function: ";
    cout << test1[3];

    cout << "\nCopy Constructor test: done\n";
    flat_map<int, int> test2(test1);

    cout<< "[]operator test: " << test2[3];

    test2.insert(std::make_pair(3, 9));

    cout << ", " << (test1 == test2);

    cout << ", " << test1.at(3);
    

    cout <<"\nTesting find function: ";

    cout << test2.find(3)->second;

    cout << "\ntesting emplace: ";
    test1.emplace(4, 7);
    cout << test1[4];

    cout << "\nTesting Erase: ";
    cout << test1.size();
    test1.erase(3);
    cout<<", " << test1.size();

    cout <<"\nTesting lower_bound: ";
    cout << test1.lower_bound(3)->first;
    
    cout <<"\nTesting upper_bound: ";
    cout << test1.upper_bound(3)->first;

    return 0;
}

