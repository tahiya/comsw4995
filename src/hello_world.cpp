#include <iostream>
#include "flat_map.hpp"

using namespace std;

int main() {

    flat_map<int, char> map;
    string s = "HELLOWORLD";
    for(int i = 0;  i < 10; i++){
        map.emplace(i, s[i]);
    }
    for(int i = 0; i < 10; i++){
        cout << map[i] << '\n';
    }

    return 0;
}
