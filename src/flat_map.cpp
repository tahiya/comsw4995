
struct compare
{
    bool operator()(const pair_type& a, const pair_type& b)
    {
        return a.first < b.first;
    }

    bool operator()(const pair_type& a, key_type b)
    {
        return a.first < b;
    }

    bool operator()(key_type a, const pair_type& b)
    {
        return a < b.first;
    }
};

std::iterator insert(const pair_type& pair)
{
    iterator it = std::lower_bound(data.begin(), data.end(), pair.first, compare());
    if (it == data.end() || it->first != pair.first)
    {
        iterator new_it = data.insert(it, pair);
        return new_it;
    }
    else
    {
        return it;
    }
}


V& operator[](const key& k)
{
    iterator it = std::lower_bound(data.begin(), data.end(), k, compare());
    if (it == data.end() || it->first != key)
    {
        iterator new_it = data.insert(it, pair_type(k, V()));
        return new_it->second;
    }
    else
    {
        return it->second;
    }
}