#include <iostream>
#include "./../src/flat_map.hpp"

using namespace std;


int main() {

flat_map<int, int> test;
test.insert(make_pair(1, 1));
test.insert(make_pair(2, 2));

cout << "Testing Iterators\n";
int back = -1;

assert(test.begin()->first ==  1);
assert((test.end() + back)->first == 2);
assert(test.rbegin()->first == 2);
assert((test.rend() + back)->first == 1);

cout << "tests passed!";


    return 0;
}


