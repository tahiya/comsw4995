#include <iostream>
#include "./../src/flat_map.hpp"

using namespace std;


int main() {

cout << "Testing Standard Constructor\n";

try {

    flat_map<int, int> test1;
    flat_map<int, char> test2;
    cout << "Made to end to exception caught!";

} catch(const std::exception& e) {

        cout << e.what();
    }


cout << "\nTesting Copy Constructor\n";

try {

    flat_map<int, int> test1;

    test1.insert(make_pair<int, int>(3, 8));

    flat_map<int, int> test2(test1);

    cout << "Made to end to exception caught!";

} catch(const std::exception& e) {

        cout << e.what();
    }

    return 0;

}


