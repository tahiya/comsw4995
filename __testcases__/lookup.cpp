#include <iostream>
#include "./../src/flat_map.hpp"

using namespace std;


int main() {

cout << "Testing flat_map Lookup\n";



    flat_map<int, int> test;
    test.emplace(3, 3);
    test.insert(std::make_pair(4, 10));
    test.insert(std::make_pair(5, 11));

    assert((test.find(3)->first) == 3);
    
    // Returns an iterator pointing to the first element in the range [first, last) that is not less than (i.e. greater or equal to) value, or last if no such element is found.
    assert((test.lower_bound(3)->first) == 3);

    // Returns an iterator pointing to the first element in the range [first, last) that is greater than value, or last
    assert((test.upper_bound(4)->first) ==  5);

    cout << "Find: tests passed!";
    
    return 0;
}
