#include <iostream>
#include "./../src/flat_map.hpp"

using namespace std;

int main() {
    
    flat_map<int, int> test;
    
    assert(test.empty() == 1);
    
    test.insert(make_pair(1, 1));
    test.insert(make_pair(2, 2));
    
    cout << "Testing Capacity Functions\n";
    
    assert(test.size() == 2);
    
    cout << test.max_size() << "\n";
    
    /*
     This is the maximum potential size the container
     can reach due to known system or library implementation limitations,
     but the container is by no means guaranteed to be able to reach that size:
     it can still fail to allocate storage at any point before that size is reached.
     */
    cout << "tests passed!"<< "\n";
    
    
    return 0;
}

