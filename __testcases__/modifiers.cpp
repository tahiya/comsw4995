#include <iostream>
#include "./../src/flat_map.hpp"

using namespace std;


int main() {

cout << "Testing flat_map Modifiers Operations:\n";

    flat_map<int ,char> test;
    
    test.insert(make_pair(4, 4));
    assert(test[4] == 4);

    test.emplace(3, 7);
    assert(test[3] == 7);


    test.erase(3);

    try {
        test.at(3);

    } catch(string s) {
        cout << "\nThe following error should be thrown: ";
        cout << s;
        cout << "\nerase test: passed!";
    }

    flat_map<int, int> a;
    flat_map<int, int> b;

    a.emplace(1, 1);
    b.emplace(2, 2);

    a.swap(b);

    assert(a[2] == 2);
    assert(b[1 == 1]);

    test.clear();

    assert(test.size() == 0);

    cout << "\n\nAll tests passed!";


    

    return 0;
}


