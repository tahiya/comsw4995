#include <iostream>
#include "./../src/flat_map.hpp"

using namespace std;


int main() {

cout << "Testing flat_map Access Operation:\n";

    flat_map<int ,char> test1;
    flat_map<int, int> test2;
    
    try {

        test1.at(1);

    } catch( std::string e){
        cout << "Map is empty and following exception is correct: ";
        cout << e << '\n';
        cout << "tests passed!\n\n";
    }

    test1.insert(make_pair(1, 'H'));
    test2.insert(make_pair(3, 5));

    cout << "Tests at() with existing values contained in flat_map:\n";

    assert(test1.at(1) == 'H');
    assert(test2.at(3) == 5);

    cout << "tests passed!\n\n";

    cout << "Testing flat_map Access Operation: operator[]\n";
    flat_map<int, int> test3;

    test3.insert(make_pair(5, 5));

    assert(test3[5] == 5);
    cout << "tests passed!";

    return 0;
}


